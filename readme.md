# Стилизация html тега 'select'

## Подключения

    <link rel="stylesheet" href="/css/select_trim.min.css">

    <script src="/js/jquery.js"></script>

    <script src="/js/select_trim.min.js"></script>

## Инициализация

    $('.category').select_trim();

или с параметрами

    $('.category').select_trim({

       'animate': 'slide',

       'speed': 400

    });

### Опции

#### Анимация

    Параметр: animate

    Значения: 'slide', 'fade' (default 'slide')

#### Скорость анимации

    Параметр: speed

    Значения: число (default 300)

#### Число элементов в выпадающем списке

    Параметр: max_item

    Значения: число (default 6)

### Методы

#### События по клику на элемент списка

    Параметр: onClick

    Значения: function(e){}

        e - элемент по каторому был клик.

## Скриншоты

![](http://ltrim.ru/bitbucket/select.png)