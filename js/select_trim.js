(function ($) {

    $.fn.select_trim = function (o) {
        var select = $(this);
        var array = [];
        var select_trim,
            list,
            text;
        var option = function () {
            var option = {};
            if(!o){
                option.animate = 'slide';
                option.speed = 300;
                option.max_item = 6;
                option.dynamism = 'none';
            } else {
                option.animate = o.animate ? o.animate : 'slide';
                option.speed = o.speed ? o.speed : 300;
                option.max_item = o.max_item ? o.max_item : 6;
                option.dynamism = o.dynamism ? o.dynamism : 'none';
            }
            return option;
        };
        var dynamism = function () {

        };
        var array_init = function() {
            var option = [];
            select.find('option').each(function (i, v) {
                var value = $(v).html(),
                    name = $(v).attr('value'),
                    selected = 0;
                var def = {
                    'name': name,
                    'value' : value,
                    'selected' : selected
                };
                if($(v).is('[selected]')){
                    array['default_text'] = def;
                }
                option[i] = def;
            });
            array['option'] = option;
        };
        var block_init = function() {
            select.css('display', 'none');
            select.after('<div class="select_trim"></div>');
            select_trim = select.next();
            select_trim.append("<div class='text'></div>");
            select_trim.append("<div class='list'></div>");
            list = select_trim.find('.list');
            text = select_trim.find('.text');
            setTimeout(function () {
                list.css({
                    'top': text.innerHeight() + 2,
                    'width': text.innerWidth()
                });
            }, 10);
            text.append("<span>"+array.default_text.value+"</span>");
            text.append("<i></i>");
            $.each(array.option, function (i, v) {
                list.append('<div data-id="'+v.name+'" class="'+(v.selected == 1 ? 'active' : '')+'">'+v.value+'</div>')
            });
            list.find('div[data-id='+array.default_text.name+']').addClass('active');
            var max_height = list.find('.active').innerHeight() * option().max_item;
            list.css({
                'display': 'none',
                'max-height': max_height
            });
        };
        var animation = function(elm, type) {
            if(type == 'Up'){
                switch (option().animate){
                    case 'slide':
                        elm.slideUp(option().speed);
                        break;
                    case 'fade':
                        elm.fadeOut(option().speed);
                        break;
                }
            }
            if(type == 'Down'){
                switch (option().animate){
                    case 'slide':
                        elm.slideDown(option().speed);
                        break;
                    case 'fade':
                        elm.fadeIn(option().speed);
                        break;
                }
            }
        };
        var onclick_init = function() {
            text.on('click', function () {
                if($(this).hasClass('active')){
                    animation(list, 'Up');
                    $(this).removeClass('active');
                } else {
                    animation(list, 'Down');
                    $(this).addClass('active');
                }
            });
            select_trim.parents(document).on('click', function (e) {
                // var container = select_trim;
                // // console.log(container);
                if (select_trim.has(e.target).length === 0){
                    animation(list, 'Up');
                    text.removeClass('active');
                }
            });
            list.find('div').on('click', function () {
                list.find('div').removeClass('active');
                $(this).addClass('active');
                animation(list, 'Up');
                text.removeClass('active');
                text.find('span').html($(this).html());
                select.find('option').removeAttr('selected');
                select.find('option[value='+$(this).attr('data-id')+']').attr('selected', '');
                if (o.onClick && typeof o.onClick === 'function') {
                    o.onClick($(this))
                }
            })
        };
        array_init();
        block_init();
        onclick_init();
    }

})(jQuery);
