<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>Пользователь</title>
    <link rel="stylesheet" href="/css/select_trim.min.css">
</head>
<body>
<div class="block"></div>
<div class="list">
    <select name="category" class="category">
        <option selected value="games">Игры</option>
        <option value="films">Фильмы</option>
        <option value="book">Книги</option>
        <option value="sport">Спорт</option>
        <option value="otd">Отдых</option>
        <option value="tue">Туризм</option>
        <option value="ed">Еда</option>
    </select>
</div>
<div class="block"></div>
<script src="/js/jquery.js"></script>
<script src="/js/select_trim.min.js"></script>
<script>
    $(document).ready(function () {
        $('.category').select_trim();
    })
</script>
</body>
</html>