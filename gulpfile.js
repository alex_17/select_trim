'use strict';

var gulp       = require('gulp'),
    sass       = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify     = require('gulp-uglify'),
    rename     = require('gulp-rename'),
    concat = require('gulp-concat');

/* АВТОМАТИЧЕСКИЕ ГЕНЕРАТОРЫ */

// Стили дисктопной версии
var css = [
    'css/select_trim.scss'
];
gulp.task('sass', function () {
    gulp.src(css)
        .pipe(concat('select_trim.css'))
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('css'));
});
gulp.task('sass:watch', function () {
    gulp.watch(css, ['sass']);
});

// Скрипты дисктопной версии
var script = [
    'js/select_trim.js'
];
gulp.task('script', function () {
    gulp.src(script)
        .pipe(concat('select_trim.js'))
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('js'));
});
gulp.task('script:watch', function () {
    gulp.watch(script, ['script']);
});

// Главная сборка
gulp.task('watch', ['sass:watch', 'script:watch']);